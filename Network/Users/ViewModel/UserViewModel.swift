//
//  UserViewModel.swift
//  Network
//
//  Created by Arun Joseph on 17/09/21.
//

import Foundation

protocol UserViewModelDelegate: AnyObject {
    func didFinish()
    func didFailed(with error: String)
}

class UserViewModel {
    
    weak var delegate: UserViewModelDelegate?
    var userList: [User] = []
    
    func fetchUserList() {
        let request = UserRouter.getUserList
        ApiManager.shared.request(request, responseType: [User].self) {[weak self] result in
            switch result {
            case .success(let users):
                self?.userList = users
                self?.delegate?.didFinish()
            case .failure(let error):
                self?.delegate?.didFailed(with: error.localizedDescription)
            }
        }
    }
}
