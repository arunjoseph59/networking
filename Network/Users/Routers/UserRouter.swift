//
//  UserRouter.swift
//  Network
//
//  Created by Arun Joseph on 16/09/21.
//

import Foundation

import Foundation
import Alamofire

enum UserRouter: URLRequestBuilder {
    
    case getUserList
    
    var endpoint: EndPoint {
        switch self {
        case .getUserList: return .getusers
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getUserList: return .get
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let request = baseRequest
        return request
    }
}
