//
//  ViewController.swift
//  Network
//
//  Created by Arun Joseph on 16/09/21.
//

import UIKit

class UserViewController: UIViewController {

    @IBOutlet weak var userListTableView: UITableView!
    
    private let viewModel = UserViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        registerCells()
        fetchUser()
    }
    
    private func fetchUser() {
        viewModel.fetchUserList()
    }
    
    private func registerCells() {
        userListTableView.register(cell: UserTableViewCell.self)
    }
}

//MARK:- UITableViewDataSource, UITableViewDelegate
extension UserViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = userListTableView.dequeue(cell: UserTableViewCell.self, forIndexPath: indexPath)
        cell.nameLabel.text = viewModel.userList[indexPath.row].name
        cell.numberLabel.text = viewModel.userList[indexPath.row].phone
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension UserViewController: UserViewModelDelegate {
    func didFinish() {
        userListTableView.reloadData()
    }
    
    func didFailed(with error: String) {
        print(error)
    }
}
